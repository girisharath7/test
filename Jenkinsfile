#!groovy

def tagsCommand = ""
def buildIpaStatus = ""
def appUrl = ""
def response
def projectId
def pipelineId
def finalStatus
def autoStatus
pipeline{
  agent{
    node{
      label 'seit-swarm'
    }
  }

//   parameters{
//     choice(name: 'BUILD_IPA', choices: ['yes' , 'no'], description: 'choose yes if you want to build the ipa or use appId instead')
//     choice(name: 'APP_ID', defaultValue:'39', description: 'select app id that want to use (only for using saucelabs)')
//     choice(name: 'ENVIRONMENT_TEST', choices: ['dev' , 'staging'], description: 'specify the environment test (dev, staging, etc)')
//     string(name: 'TAGS', defaultValue:'@SampleFeature and (@1 or @2)', description: 'specify the cucumber tags to run')
//     string(name: 'THREAD_COUNT', defaultValue:'1', description: 'specify how many thread for running automation test simultaneously')
//     string(name: 'PROPS_FILE', defaultValue:'testobject', description: 'Specify the properties file that you want to used (ex: application.properties to be application)')
//   }
  environment{
    BUILD_PROJECT_NAME = "SEIT/IOS_Build/moka-ios-build-test";
  }
  stages{
    stage('print parameters'){
      steps{
        script{
          println "Parameters : ${params}"
        }
      }
    }
    stage('Build ipa'){
      when{
          // Only perform this stage when BUILD_IPA is 'yes'
          expression { params.BUILD_IPA.equalsIgnoreCase('yes') }
      }
      steps{
        script{
          withCredentials([string(credentialsId: 'iOSBuildAppToken', variable: 'BUILD_TOKEN')]){
            response = sh(script: 'curl -X POST "https://source.golabs.io/api/v4/projects/22519/trigger/pipeline" -F "token=${BUILD_TOKEN}" -F "ref=develop" -F "variables[SHOULD_UPLOAD_TO_BROWSERSTACK]=true"', returnStdout: true)
          }
          if(response == null) {
            error "Trigger gitlab to build ipa is failed. Please try again"
          }
          def resData = new groovy.json.JsonSlurper().parseText(response)
          println resData
          projectId = resData.project_id
          pipelineId = resData.id
        }
      }
    }
    stage('Upload to browserstack') {
      when{
          // Only perform this stage when BUILD_IPA is 'yes'
          expression { params.BUILD_IPA.equalsIgnoreCase('yes') }
      }
      steps{
        script{
          String url = "https://source.golabs.io/api/v4/projects/${projectId}/pipelines/${pipelineId}"
          def max_attempts = 15
          for (i = 1; i <= max_attempts; i++) {
            withCredentials([string(credentialsId: 'iOSPrivateToken', variable: 'PRIVATE_TOKEN')]){
              result = sh(script:'curl --location --request GET "'+url+'" --header '+"PRIVATE-TOKEN:${PRIVATE_TOKEN}"+'', returnStdout: true)
              response = result
              def resData = new groovy.json.JsonSlurper().parseText(response)
              println "build process is ${resData.status}"
              buildIpaStatus = resData.status
            }
            if(buildIpaStatus.equalsIgnoreCase("canceled") || buildIpaStatus.equalsIgnoreCase("success") || buildIpaStatus.equalsIgnoreCase("failed"))
            break;
            sleep(time: 3, unit: 'MINUTES')
          }
          println buildIpaStatus
          if(buildIpaStatus.equalsIgnoreCase("canceled") || buildIpaStatus.equalsIgnoreCase("failed")) {
            error "Build ipa process is ${buildIpaStatus}. Check the gitlab pipeline on https://source.golabs.io/go-merchants/pos/mokapos/mobile/ios/moka-ios/-/pipelines/${pipelineId}"
          } else {
            echo "Build is successful!"
          }
        }
      }
    }
    stage('Get app') {
      steps{
        script{
          withCredentials([string(credentialsId: 'iOSSauceLabsUser', variable: 'SAUCELABS_USER'),string(credentialsId: 'iOSSauceLabsPassword', variable: 'SAUCELABS_PASSWORD')]){
            response = sh(script: 'curl -u "${SAUCELABS_USER}:${SAUCELABS_PASSWORD}" -X GET "https://api-cloud.browserstack.com/app-automate/recent_apps/"', returnStdout: true)
          }
          def json = new groovy.json.JsonSlurper().parseText(response)
            json.eachWithIndex { data, idx ->
            if(data.getAt("app_name").contains(".ipa") && appUrl == "") {
              appUrl = data.getAt("app_url")
            }
          }
          println appUrl
        }
      }
    }
    stage('Run execution AT test'){
      steps{
        script{
          def buildTriggerBy = "${currentBuild.getBuildCauses()[0].userName}"
          def propertyFile = "${env.WORKSPACE}/src/test/resources/browserstack.properties"
          def jobDetail = "job:${JOB_BASE_NAME};build_id:${BUILD_ID}"
          withCredentials([string(credentialsId: 'iOSSauceLabsUser', variable: 'BROWSERSTACK_USER'),string(credentialsId: 'iOSSauceLabsPassword', variable: 'BROWSERSTACK_KEY')]){
              sh 'sed -i "s+driver.mobile.ios.appiumUrl=.*+driver.mobile.ios.appiumUrl=http://${BROWSERSTACK_USER}:${BROWSERSTACK_KEY}@hub-cloud.browserstack.com/wd/hub+g" "'+propertyFile+'" '
          }
          sh 'sed -i -e "s/appiumVersion/' + params.APPIUM_VERSION + '/" "' + propertyFile + '" '
          sh 'sed -i "s+driver.mobile.ios.capabilities.app=.*+driver.mobile.ios.capabilities.app="'+appUrl+'"+g" "'+propertyFile+'" '
          sh 'sed -i -e "s/deviceName/' + params.DEVICE_NAME + '/" "' + propertyFile + '" '
          sh 'sed -i -e "s/buildName/' + "CI_MOKA-IOS_AT [TriggerBy:${buildTriggerBy}][JobName:${JOB_BASE_NAME}][BuildID:${BUILD_ID}]" + '/" "' + propertyFile + '" '
          if (params.TESTRUN_ID != '') {
            sh 'sed -i "s+testrail.updateTestResult=.*+testrail.updateTestResult="true"+g" "'+propertyFile+'"'
            sh 'sed -i "s+testrail.runId=.*+testrail.runId="'+params.TESTRUN_ID+'"+g" "'+propertyFile+'"'
          }
          sh 'cat src/test/resources/'+ params.PROPS_FILE +'.properties'
          if (params.TAGS.isEmpty()) {
            catchError {
              autoStatus = sh returnStatus: true, script: "./gradlew clean -Drp.launch=\"${params.RP_LAUNCH}\" -Drp.project=\"${params.RP_PROJECT}\" -Drp.attributes=\"${jobDetail}\" cucumber -Penv=\"${params.ENVIRONMENT_TEST}\" -Pprops=\"${params.PROPS_FILE}\" --threads ${params.THREAD_COUNT}"
            }
          } else {
            catchError {
              autoStatus = sh returnStatus: true, script: "./gradlew clean -Drp.launch=\"${params.RP_LAUNCH}\" -Drp.project=\"${params.RP_PROJECT}\" -Drp.attributes=\"${jobDetail}\" cucumber -Penv=\"${params.ENVIRONMENT_TEST}\" -Pprops=\"${params.PROPS_FILE}\" --tags \"${params.TAGS}\" --threads ${params.THREAD_COUNT}"
            }
          }
          finalStatus = autoStatus
        }
      }
      post {
        always {
          ansiColor('xterm') {
            sh 'cp build/cucumber.json build/first.json'
            sh 'rm build/cucumber.json'
          }
        }
      }
    }

    stage('Rerun Failed Test') {
      when {
        environment name: 'RERUN', value: 'true'
      }

      steps{
        script{
          if ( autoStatus != 0 ) {
            def jobDetail = "job:${JOB_BASE_NAME};build_id:${BUILD_ID}"
            finalStatus = sh returnStatus: true, script: "./gradlew -Drp.launch=\"${params.RP_LAUNCH}\" -Drp.project=\"${params.RP_PROJECT}\" -Drp.attributes=\"${jobDetail}\" -Drp.rerun=true rerunCucumber -Penv=\"${params.ENVIRONMENT_TEST}\" -Pprops=\"${params.PROPS_FILE}\" -Dcucumber.options=\"--threads ${params.THREAD_COUNT}\""
          }
        }
      }
      post {
        always {
          script {
            ansiColor('xterm') {
              if( autoStatus != 0 ) {
                sh 'cp build/cucumber.json build/rerun.json'
                sh 'rm build/cucumber.json'
              }
            }
          }
        }
      }
    }
  }

  post{
    always{
      script {
        String timestamp = currentBuild.startTimeInMillis
        automation_date = timestamp.substring(0,10)

        if (currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null ) {
          automation_trigger = currentBuild.getBuildCauses()[0].userName
        } else if (currentBuild.getBuildCauses()[0].upstreamProject != null ) {
          automation_trigger = "Upstream " + currentBuild.getBuildCauses()[0].upstreamProject
        } else if (currentBuild.getBuildCauses('hudson.triggers.TimerTrigger$TimerTriggerCause') != null ) {
          automation_trigger = "Scheduler"
        }  else {
          automation_trigger = "Unknown"
        }

        if(params.RERUN && autoStatus != 0){
          sh 'java -jar libs/malbec.jar merge build/first.json build/rerun.json'
          sh 'mv merged-cucumber.json build/final-report.json'
        } else {
          sh 'mv build/first.json build/final-report.json'
        }

        if(finalStatus == 1){
          currentBuild.result = 'FAILURE'
        } else {
          currentBuild.result = 'SUCCESS'
        }
      }

      cucumber jsonReportDirectory: 'build', fileIncludePattern: 'final-report.json'

      archiveArtifacts artifacts: 'build/*.json,target/*.xml,rerun/*.txt', fingerprint: true

      withCredentials([string(credentialsId: 'REPORT_PORTAL_API_KEY', variable: 'REPORT_PORTAL_API_KEY')]) {
       sh """
         curl -X POST http://seit.mokapos.io:2323/report/add_new \
         -F automation_name=${JOB_BASE_NAME} \
         -F automation_status=${currentBuild.currentResult} \
         -F automation_date=${automation_date} \
         -F automation_trigger=\"${automation_trigger}\" \
         -F automation_url=\"${BUILD_URL}\" \
         -F automation_build_id=${BUILD_ID} \
         -F report_portal_launch_name=${RP_LAUNCH} \
         -F report_portal_project_name=${RP_PROJECT} \
         -F report_portal_api_key=${REPORT_PORTAL_API_KEY} \
         -F save_to_db=${SAVE_TO_DB} \
         -F slack_channels=${SLACK_CHANNELS}\
         -F report_file=\"@build/final-report.json\"
       """
     }
    }
  }
}
